<?php

namespace Coobix\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CoobixUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
