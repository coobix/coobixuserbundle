<?php

namespace Acme\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Acme\UserBundle\Entity\User;

class RegistrationFormType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $roleName = User::$roleName;


        $builder
                ->add('name', null, array('label' => 'Nombre:'))
                ->add('lastname', null, array('label' => 'Apellido:'))
                
                ->add('roles', 'choice', array(
                    'label' => 'Rol:',
                    'choices' => $roleName,
                    'expanded' => true,
                    'multiple' => true                
                ))
                
                ->add('submit', 'submit', array('label' => 'CREAR'));
        ;
    }

    public function getParent() {
        return 'fos_user_registration';
    }

    public function getName() {
        return 'acme_user_registration';
    }

}
