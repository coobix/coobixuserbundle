<?php

namespace Acme\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Acme\UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegistrationFormType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $roleName = User::$roleName;

        $builder
                ->add('name', null, array('label' => 'Nombre:'))
                ->add('lastname', null, array('label' => 'Apellido:'))
                
                ->add('roles', ChoiceType::class, array(
                    'label' => 'Rol:',
                    'choices' => $roleName,
                    'expanded' => true,
                    'multiple' => true                
                ))
                ->add('submit', SubmitType::class, array('label' => 'CREAR'))
        ;
    }

    public function getParent() {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }
    
    public function getBlockPrefix()
    {
        return 'acme_user_registration';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }

}
