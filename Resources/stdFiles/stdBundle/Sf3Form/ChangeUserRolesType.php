<?php

namespace Acme\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Acme\UserBundle\Entity\User;

class ChangeUserRolesType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $roleName = User::$roleName;
        
        
        $builder
                ->add('roles', 'choice', array(
                    'label' => 'Rol:',
                    'choices' => $roleName,
                    'expanded' => true,
                    'multiple' => true                
                ))
                ->add('Aplicar','submit')
            
            ;
        
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            //'data_class' => 'Coobix\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'changeuserrolestype';
    }

}
