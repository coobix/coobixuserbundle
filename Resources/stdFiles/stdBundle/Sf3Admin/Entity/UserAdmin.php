<?php

namespace Acme\UserBundle\Admin\Entity;

use Acme\UserBundle\Form\ChangeUserRolesType;
use Coobix\UserBundle\Admin\Entity\UserAdmin as BaseAdmin;

class UserAdmin extends BaseAdmin
{
    public function __construct($class) {
        $this->setChangeUserRolesForm(ChangeUserRolesType::class);

        parent::__construct($class);
    }
}
