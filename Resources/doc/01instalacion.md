Instalación de UserAdminBundle
==================================

## Prerequisitos

## Instalación

1. Descargar UserAdminBundle usando composer
2. Habilitar el bundle


### Paso 1: Descargar UserAdminBundle usando composer

Agregar el repositorio del bundle en composer.json:

``` bash
"repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:coobix/coobixuserbundle.git"
        }
    ]
```

Agregar el bundle con el siguiente comando:

``` bash
$ composer require coobix/user-bundle "dev-master"
```

Composer instalara el  bundle en el directorio `vendor/coobix`.

### Paso 2: Habilitar el bundle

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Coobix\UserBundle\CoobixUserBundle(),
    );
}
```

### Paso 4: Configurar el bundle

# TODO: Buscar la forma de que esto lo tenga el bundle y no el config.yml

``` yaml
# app/config/config.yml
fos_user:
    db_driver: orm # other valid values are 'mongodb', 'couchdb' and 'propel'
    firewall_name: main  
    # Esto luego hay que cambiarlo en el caso que se extienda el bundle
    #user_class: Coobix\UserBundle\Model\User
    user_class: Acme\UserBundle\Entity\User
    registration:
        form:
            type: Acme\UserBundle\Form\RegistrationFormType
        confirmation:
            enabled: true
    #SF 3.4
    service:                               
        mailer: fos_user.mailer.twig_swift
    resetting:
        #email:
        #    template: CoobixUserBundle:User:resetting.email.html.twig
    from_email:
        address: noreply@domain.com
        sender_name: Algun nombre para el titulo del email
```

### Paso 5: Configurar el security de Symfony con el user bundle

``` yaml
# app/config/security.yml

# you can read more about security in the related section of the documentation
# http://symfony.com/doc/current/book/security.html
security:
    # http://symfony.com/doc/current/book/security.html#encoding-the-user-s-password
    encoders:
        #Symfony\Component\Security\Core\User\User: plaintext
        FOS\UserBundle\Model\UserInterface: bcrypt

    # http://symfony.com/doc/current/book/security.html#hierarchical-roles
    role_hierarchy:
        ROLE_ADMIN:       ROLE_USER
        ROLE_SUPER_ADMIN: [ROLE_USER, ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]

    # http://symfony.com/doc/current/book/security.html#where-do-users-come-from-user-providers
    providers:
        #in_memory:
        #    memory:
        #        users:
        #            user:  { password: userpass, roles: [ 'ROLE_USER' ] }
        #            admin: { password: adminpass, roles: [ 'ROLE_ADMIN' ] }
        fos_userbundle:
            #id: fos_user.user_provider.username
            id: fos_user.user_provider.username_email

    # the main part of the security, where you can set up firewalls
    # for specific sections of your app
    firewalls:
        main:
            pattern: ^/
            form_login:
                provider: fos_userbundle
                #SF <3
                csrf_provider: form.csrf_provider
                #SF 3+
                csrf_token_generator: security.csrf.token_manager
                
                login_path: /admin/login
                check_path: /admin/login_check
                always_use_default_target_path: false
                default_target_path: /admin/
            logout:
                path: /admin/logout
                target: /admin/
            anonymous:    true
        # disables authentication for assets and the profiler, adapt it according to your needs
        dev:
            pattern:  ^/(_(profiler|wdt)|css|images|js)/
            security: false
        # the login page has to be accessible for everybody
        #demo_login:
        #    pattern:  ^/demo/secured/login$
        #    security: false

        # secures part of the application
        #demo_secured_area:
        #    pattern:    ^/demo/secured/
            # it's important to notice that in this case _demo_security_check and _demo_login
            # are route names and that they are specified in the AcmeDemoBundle
        #    form_login:
        #        check_path: _demo_security_check
        #        login_path: _demo_login
        #    logout:
        #        path:   _demo_logout
        #        target: _demo
            #anonymous: ~
            #http_basic:
            #    realm: "Secured Demo Area"

    # with these settings you can restrict or allow access for different parts
    # of your application based on roles, ip, host or methods
    # http://symfony.com/doc/current/cookbook/security/access_control.html
    access_control:
        #- { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY, requires_channel: https }
        - { path: ^/admin/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/admin/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/admin/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/admin/, role: ROLE_ADMIN }
```

### Step 3: Importar archivos de rutas


 SE EXTIENDE EL CoobixUserBundle:

#Generar el bundle que va a extender a CoobixUserBundle


``` bash
$ php bin/console generate:bundle
```

``` bash
$ Bundle namespace: Acme/UserBundle
```



PHP:
``` php
# src/Acme/UserBundle/AcmeUserBundle.php
public function getParent()
    {
        return 'CoobixUserBundle';
    }
```

#Generar entidad User o copiar de Coobix/UserBundle/Resources/stdBundle/Entity/

# Si se genera la entidad User

``` bash
$ php bin/console doctrine:generate:entity
```

``` bash
The Entity shortcut name: AcmeUserBundle:User 
```


#Tiene que extender a la clase User de FOS y tener el metodo que esta descripto

PHP:
``` php
<?php
# src/Acme/UserBundle/Entity/User.php
//..
use FOS\UserBundle\Model\User as BaseUser;
//..
/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="Acme\UserBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser {
//..

public function getRolesName() {

        $roles = array();
        foreach ($this->getRoles() as $r) {

            if (array_key_exists($r, self::$roleName)) {
                $roles[] = self::$roleName[$r];
            }
        }
        return join(', ', $roles);
    }


```


YAML:
``` yaml
# app/config/routing.yml
acme_user:
    resource: "@AcmeUserBundle/Resources/config/routing.yml"
    prefix:   /admin
```

YAML:
``` yaml
# src/Acme/UserBundle/Resources/config/routing.yml
coobix_user:
    resource: "@CoobixUserBundle/Resources/config/routing.yml"
    prefix:   /
```

### Step 4: Crear el primer usuario

``` bash
$ php bin/console fos:user:create --super-admin
```

``` bash
Please choose a username:testing
Please choose an email:testing@testing.com
Please choose a password:testing124#
```

### Step 5: Cambiar título y traducciones

Copiar el archivo 
vendor/coobix/user-bundle/Coobix/UserBundle/Resources/translations/FOSUserBundle.es.yml
a
src/Acme/UserBundle/Resources/translations/FOSUserBundle.es.yml

Y hacer las modificaciones, pertinentes.


### Step 8: Habilitar Captcha

``` yml
# app/config/config.yml
parameters: 
    #...
    recaptcha_secret: null
    recaptcha_sitekey: null

twig:
    #...
    globals:
        #...
        recaptcha_sitekey: %recaptcha_sitekey% 
```

### Step 8: Habilitar Remember me

``` yml
# app/config/security.yml
firewalls:
        main:
            #....
            remember_me:
                secret:      "%secret%"
                lifetime: 31536000 # 365 days in seconds
                path:     /
                domain:   ~ # Defaults to the current domain from $_SERVER
```


# Step 5: User Bundle y Admin Bundle

### Step 5: Descomentar el item de usuarios del menu en el admin bundle

Si no existe el item agregar:

``` twig
# src/Acme/AdminBundle/Resources/Views/Admin/sidebar.html.twig
{% if (is_granted('ROLE_SUPER_ADMIN'))  %}
<li>
    <a href="{{ path('admin_list', {'class': 'user'}) }}">Usuarios</a>
</li>
{% endif %}
```




### Step 6: Copiar formulario de registración

Copiar el archivo 
vendor/coobix/user-bundle/Coobix/UserBundle/Resources/stdFiles/stdBundle/Form/RegistrationFormType.php
a
src/Acme/UserBundle/Form/

Cambiar "Acme" por nombre del bundle

### Step 7: Agregar formulario como servicio

``` yml
# app/config/services.yml
acme.form.registration:
    class: Acme\UserBundle\Form\RegistrationFormType
    tags:
        - { name: form.type, alias: acme_user_registration }
```

