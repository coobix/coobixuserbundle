$(document).ready(function () {

    //ESCONDE MENSAJE FLASH INFO
    $('.alert').delay(3600).fadeOut('slow');

    //CHECKEA/DESCHECKEA CHECKS DEL LISTADO
    var bulkChecks = $('input[name="bulkCheck"]');
    $('#checkAllbulkChecks').on('click', function () {

        if ($(this).is(':checked')) {
            bulkChecks.each(function () {
                $(this).prop('checked', true);
            });
        }
        else {
            bulkChecks.each(function () {
                $(this).prop('checked', false);
            });
        }



        var bulkChecksChecked = $('input[name="bulkCheck"]:checked');

        var isOneChecked = bulkChecksChecked.length;

        if (isOneChecked > 0) {
            bulkActionsCont.slideDown("slow");
        }
        else if (isOneChecked == 0) {
            bulkActionsCont.slideUp("slow");
        }

    });

    $('input[name="bulkCheck"]').on('click', function (e) {
        var bulkChecksChecked = $('input[name="bulkCheck"]:checked');

        var isOneChecked = bulkChecksChecked.length;

        if (isOneChecked > 0) {
            bulkActionsCont.slideDown("slow");
        }
        else if (isOneChecked == 0) {
            bulkActionsCont.slideUp("slow");
        }

    });




    //VALIDA ENVIO DEL FORMULARIO BULK
    $('#listBulkForm').on('submit', function (e) {

        var bulkChecksChecked = $('input[name="bulkCheck"]:checked');

        var isOneChecked = bulkChecksChecked.length;

        if (isOneChecked > 0) {

            var ids = 0;

            bulkChecksChecked.each(function () {
                ids += ',' + $(this).val();
            });
            $('#bulkIds').val(ids);
        }
        else {
            alert('Por favor seleccione algun item del listado.\n')
            e.preventDefault();
        }

    });



    $('.delete-form').on('click', function (e) {
        e.preventDefault();
        var deleteForm = $(this);

        bootbox.dialog({
            message: "¿Desea eliminar el registro seleccionado?",
            title: "Confirmar operación",
            buttons: {
                danger: {
                    label: "Cancelar",
                    className: "btn-danger",
                    callback: function () {

                    }
                },
                main: {
                    label: "Confirmar",
                    className: "btn-primary",
                    callback: function () {
                        deleteForm.submit();
                    }
                }
            }
        });

    });


    $('.confirm-delete').on('click', function (e) {
        e.preventDefault();
        //console.log($('#myModal').data('bs.modal').options.source);
    });


    var busquedaAvanzadaLink = $('#busquedaAvanzadaLink');
    var busquedaAvanzadaCont = $('#busquedaAvanzadaCont');

    //MUESTRA EL SELECT DE BUSQUEDA AVANZADA    
    busquedaAvanzadaLink.click(function () {
        busquedaAvanzadaCont.slideToggle("slow");
    });

    var bulkActionsLink = $('#bulkActionsLink');
    var bulkActionsCont = $('#bulkActionsCont');

    //MUESTRA EL SELECT DE ACCIONES EN BLOQUE   
    bulkActionsLink.click(function () {
        bulkActionsCont.slideToggle("slow");
    });

/*
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    
    $.datepicker.setDefaults($.datepicker.regional['es']);


    $(".datepicker").datepicker({
        dateFormat: "dd-mm-yy"
    });
*/


});
