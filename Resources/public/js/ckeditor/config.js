/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function(config) {
// Define changes to default configuration here. For example:
// config.language = 'fr';
// config.uiColor = '#AADC6E';
    config.language = 'es';

    config.toolbar = [
        {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['NewPage', 'Preview', ]},
        {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
        {name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
        '/',
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
        {name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar', 'PageBreak']},
        '/',
        {name: 'styles', items: ['Font', 'FontSize']},
        {name: 'colors', items: ['TextColor']},
        {name: 'tools', items: ['Maximize']},
        {name: 'others', items: ['-']},
    ];

    //config.contentsCss = 'fonts.css';
//the next line add the new font to the combobox in CKEditor
    config.font_names = 'Ryman Eco/Ryman Eco;' + config.font_names;
    config.font_defaultLabel = 'Ryman Eco';

};