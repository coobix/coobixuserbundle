$(document).ready(function () {

    //Modal
    var loginModal = $('#loginModal');
    loginModal.modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });

    //Evento del boton cerrar
    loginModal.find('#closeLoginModalBtn').on('click', function () {
        var url = Routing.generate('fos_user_security_logout', {}, true);
        $(location).attr('href', url);
    });

    //Evento del boton Entrar
    loginModal.find('#goLoginModalBtn').on('click', function () {

        var me = $(this);
        me.prop('disabled', true);
        var form = loginModal.find('form');

        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize(),
            success: function (data) {
                if (typeof data.logged !== 'undefined') {
                    loginModal.find('#formCont').html(data.form);
                }
                else {
                    loginModal.modal('hide');
                }
                me.prop('disabled', false);
            }
        });
    });


    $(document).on('mouseenter', function () {
        checkSession();
    });

    function checkSession() {
        if (loginModal.data('bs.modal').isShown) {
            return;
        }
        $.ajax({
            url: Routing.generate('user_checksession_admin', {}, true),
            success: function (data) {
                if (!data.logged) {
                    loginModal.modal('show');
                    loginModal.find('#formCont').html(data.form);
                }
            }
        });
    }

});
