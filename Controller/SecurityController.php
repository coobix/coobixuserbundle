<?php

namespace Coobix\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Coobix\UserBundle\Controller\CoobixSecurityHandler;

class SecurityController extends BaseController
{

    public function loginAction(Request $request) {
        $symfonyVersion = explode('.', \Symfony\Component\HttpKernel\Kernel::VERSION);
        $response = $this->forward('CoobixUserBundle:Sf'.$symfonyVersion[0].'Security:login', array());
        return $response;
    }

}
