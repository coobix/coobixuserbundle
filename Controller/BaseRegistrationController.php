<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Coobix\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Controller\RegistrationController as BaseController;

/**
 * Controller managing the registration
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class BaseRegistrationController extends BaseController 
{

    public function registerAction(Request $request) {
        $admin = $this->get('coobix.admin.user');

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);
        /*
          $event = new GetResponseUserEvent($user, $request);
          $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

          if (null !== $event->getResponse()) {
          return $event->getResponse();
          }
         */
        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            /*
              $event = new FormEvent($form, $request);
              $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
             */
            $userManager->updateUser($user);

            $url = $this->container->get('router')->generate('admin_show', array('class' => 'user', 'id' => $user->getId()));

            $this->container->get('session')->getFlashBag()->add('success', 'REGISTRO CREADO.');
            $this->get('coobix.log')->create('CREATE User: ' . $user . '. ID: ' . $user->getId());
            $response = new RedirectResponse($url);

            /*
              if (null === $response = $event->getResponse()) {
              $url = $this->container->get('router')->generate('admin_show', array('class' => 'user', 'id' => $user->getId()));


              $response = new RedirectResponse($url);
              }

              $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
             * 
             */
            return $response;
        }

        return $this->render('FOSUserBundle:Registration:register.html.twig', array(
                    'form' => $form->createView(),
                    'admin' => $admin
        ));
    }

    public function webRegisterAction(Request $request) {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        //VALIDACION CAPTCHA
        $recaptchaSecret = ($this->container->hasParameter('recaptcha_secret')) ? $this->getParameter('recaptcha_secret') : null;
        $recaptchaSiteKey = ($recaptchaSecret) ? $this->getParameter('recaptcha_sitekey') : null;
        $recaptchaResult = true;

        //Me fijo si mandaron 
        if ($recaptchaSecret) {
            $gRecaptchaResponse = $request->request->get('g-recaptcha-response', null, true);
            $remoteIp = $request->getClientIp();
            $recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecret);
            $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
            $recaptchaResult = $resp->isSuccess();
        }


        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($recaptchaResult && $form->isValid()) {

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        //Deberia haber una forma mejor de enviar el error del captcha
        $recaptchaMsg = (!$recaptchaResult && $form->isSubmitted()) ? 'Captcha Inválido' : '';

        return $this->render('FOSUserBundle:Registration:web_register.html.twig', array(
                    'form' => $form->createView(),
                    'recaptchaSiteKey' => $recaptchaSiteKey,
                    'recaptchaMsg' => $recaptchaMsg
        ));
    }

    /**
     * Receive the confirmation token from user email provider, login the user
     *
     * @param Request $request
     * @param string  $token
     *
     * @return Response
     */
    public function confirmAction(Request $request, $token) 
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            
            $url = $this->generateUrl('admin');
            return new RedirectResponse($url);
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);

        $userManager->updateUser($user);

        if (null === $response = $event->getResponse()) {
            $url = $this->generateUrl('fos_user_registration_confirmed');
            $response = new RedirectResponse($url);
        }
        
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, $response));

        return $response;
    }
}
