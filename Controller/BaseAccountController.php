<?php

namespace Coobix\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Account controller.
 *
 */
class BaseAccountController extends Controller {

    /**
     * Show Dashboard.
     *
     */
    public function indexAction() {
        return $this->redirect($this->generateUrl('user_profile_account'));
        /*
          return $this->render('CoobixUserBundle:Account:dashboard.html.twig', array(
          ));
         */
    }

    /*
     * MÉTODOS DEL PERFIL *
     * ****************** */

    public function profileAction() {

        //antes de SF 2.6
        //$user = $this->get('security.context')->getToken()->getUser();
        /*
         * #-CI 
         * aca deberias fijarte si esta logueado?
         */
        $user = $this->getUser();

        return $this->render('CoobixUserBundle:Account:profile.html.twig', array(
                    'entity' => $user,
        ));
    }

}
