<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Coobix\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Controller\RegistrationController as BaseController;

/**
 * Controller managing the registration
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class RegistrationController extends BaseController
{

    public function registerAction(Request $request) {
        $symfonyVersion = explode('.', \Symfony\Component\HttpKernel\Kernel::VERSION);
        $response = $this->forward('CoobixUserBundle:Sf'.$symfonyVersion[0].'Registration:register', array());
        return $response;
    }

    public function webRegisterAction(Request $request) {
        $symfonyVersion = explode('.', \Symfony\Component\HttpKernel\Kernel::VERSION);
        $response = $this->forward('CoobixUserBundle:Sf'.$symfonyVersion[0].'Registration:webRegister', array());
        return $response;
    }

    /**
     * Tell the user to check his email provider
     */
    public function checkEmailAction() {
        $symfonyVersion = explode('.', \Symfony\Component\HttpKernel\Kernel::VERSION);
        $response = $this->forward('CoobixUserBundle:Sf'.$symfonyVersion[0].'Registration:checkEmail', array());
        return $response;
    }

    /**
     * Receive the confirmation token from user email provider, login the user
     */
    public function confirmAction(Request $request, $token) 
    {

        $symfonyVersion = explode('.', \Symfony\Component\HttpKernel\Kernel::VERSION);
        $response = $this->forward('CoobixUserBundle:Sf'.$symfonyVersion[0].'Registration:confirm', array('token'=> $token));
        return $response;
    }


}
