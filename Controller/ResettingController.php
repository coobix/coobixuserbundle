<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Coobix\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\UserBundle\Controller\ResettingController as BaseController;

/**
 * Controller managing the resetting of the password
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class ResettingController extends BaseController
{

    /**
     * Request reset user password: show form
     */
    public function requestAction() {
       $symfonyVersion = explode('.', \Symfony\Component\HttpKernel\Kernel::VERSION);
       $response = $this->forward('CoobixUserBundle:Sf'.$symfonyVersion[0].'Resetting:request', array(
        ));
        return $response;
    }
    
    /**
     * Request reset user password: submit form and send email
     */
    public function sendEmailAction(Request $request) {

      $symfonyVersion = explode('.', \Symfony\Component\HttpKernel\Kernel::VERSION);
      $response = $this->forward('CoobixUserBundle:Sf'.$symfonyVersion[0].'Resetting:sendEmail', array());
      return $response;
    }
    
    public function successAction() {
        $symfonyVersion = explode('.', \Symfony\Component\HttpKernel\Kernel::VERSION);
      $response = $this->forward('CoobixUserBundle:Sf'.$symfonyVersion[0].'Resetting:success', array());
      return $response;
    }

}
