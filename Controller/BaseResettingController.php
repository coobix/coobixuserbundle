<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Coobix\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\UserBundle\Controller\ResettingController as BaseController;

/**
 * Controller managing the resetting of the password
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class BaseResettingController extends BaseController
{

    /**
     * Request reset user password: show form
     */
    public function requestAction() {
        //VALIDACION CAPTCHA
        $recaptchaSecret = ($this->container->hasParameter('recaptcha_secret')) ? $this->getParameter('recaptcha_secret') : null;
        $recaptchaSiteKey = ($recaptchaSecret) ? $this->getParameter('recaptcha_sitekey') : null;
        $recaptchaMsg = '';

        return $this->render('FOSUserBundle:Resetting:request.html.twig', array(
                    'recaptchaSiteKey' => $recaptchaSiteKey,
                    'recaptchaMsg' => $recaptchaMsg
        ));
    }

    /**
     * Request reset user password: submit form and send email
     */
    public function sendEmailAction(Request $request) {
        
        //VALIDACION CAPTCHA
        $recaptchaSecret = ($this->container->hasParameter('recaptcha_secret')) ? $this->getParameter('recaptcha_secret') : null;
        $recaptchaSiteKey = ($recaptchaSecret) ? $this->getParameter('recaptcha_sitekey') : null;
        $recaptchaResult = true;

        //Me fijo si mandaron 
        if ($recaptchaSecret) {
            $gRecaptchaResponse = $request->request->get('g-recaptcha-response', null, true);
            $remoteIp = $request->getClientIp();
            $recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecret);
            $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
            $recaptchaResult = $resp->isSuccess();
        }
        
        if (false === $recaptchaResult) {
            return $this->render('FOSUserBundle:Resetting:request.html.twig', array(
                        'recaptchaSiteKey' => $recaptchaSiteKey,
                        'recaptchaMsg' => 'Captcha Inválido',      
            ));
        }
        //FIN VALIDACION CAPTCHA
        
        $username = $request->request->get('username');
        
        /** @var $user UserInterface */
        $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

        if (null === $user) {
            return $this->render('FOSUserBundle:Resetting:request.html.twig', array(
                        'invalid_username' => $username,
                        'recaptchaSiteKey' => $recaptchaSiteKey,
                        'recaptchaMsg' => '',
            ));
        }

        if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->render('FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig');
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($user);

        return new RedirectResponse($this->generateUrl('fos_user_resetting_check_email', array('username' => $username)));
    }

    public function successAction() {

        return $this->render('FOSUserBundle:Resetting:success.html.twig', array(
        ));
    }

}
