<?php

namespace Coobix\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Coobix\UserBundle\Form\Account\Sf3UserAccountEditType as UserAccountEditType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Coobix\UserBundle\Controller\BaseAccountController;

/**
 * Account controller.
 */
class Sf3AccountController extends BaseAccountController {

    public function editProfileAction() {

        //antes de SF 2.6
        //$user = $this->get('security.context')->getToken()->getUser();
        /*
         * #-CI 
         * aca deberias fijarte si esta logueado?
         */
        $user = $this->getUser();

        if (!$user) {
            throw $this->createNotFoundException('NO SE PUDO ENCONTRAR LA ENTIDAD.');
        }

        $form = $this->createForm(UserAccountEditType::class, $user, array(
            'action' => $this->generateUrl('user_updateprofile_account'),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'MODIFICAR'));
        return $this->render('CoobixUserBundle:Account:edit_profile.html.twig', array(
                    'entity' => $user,
                    'form' => $form->createView(),
        ));
    }

    public function updateProfileAction(Request $request) {

        //antes de SF 2.6
        //$user = $this->get('security.context')->getToken()->getUser();
        /*
         * #-CI 
         * aca deberias fijarte si esta logueado?
         */
        $user = $this->getUser();

        if (!$user) {
            throw $this->createNotFoundException('NO SE PUDO ENCONTRAR LA ENTIDAD.');
        }

        $form = $this->createForm(UserAccountEditType::class, $user, array(
            'action' => $this->generateUrl('user_updateprofile_account'),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'MODIFICAR'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('fos_user.user_manager')->updateUser($user);
            $this->get('coobix.log')->create('PROFILE UPDATE User: ' . $user . '. ID: ' . $user->getId());
            $this->get('session')->getFlashBag()->add('success', 'REGISTRO MODIFICADO.');
            return $this->redirect($this->generateUrl('user_editprofile_account'));
        }

        $this->get('session')->getFlashBag()->add('danger', 'EL REGISTRO NO SE PUDO MODIFICAR VERIFIQUE EL FORMULARIO.');
        return $this->render('CoobixUserBundle:Account:edit_profile.html.twig', array(
                    'entity' => $user,
                    'form' => $form->createView(),
        ));
    }

}
