<?php

namespace Coobix\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class BaseSecurityController extends BaseController
{

    public function loginAction(Request $request) {

        /** @var $session \Symfony\Component\HttpFoundation\Session\Session */
        $session = $request->getSession();
        //VALIDACION CAPTCHA
        $recaptchaSecret = ($this->container->hasParameter('recaptcha_secret')) ? $this->getParameter('recaptcha_secret') : null;
        $recaptchaSiteKey = null;
        $formUrl = $this->container->get('router')->generate('fos_user_security_check');
        if ($recaptchaSecret) {
            $recaptchaSiteKey = ($this->container->hasParameter('recaptcha_sitekey')) ? $this->getParameter('recaptcha_sitekey') : null;
            $formUrl = $this->container->get('router')->generate('fos_user_security_login');
        }
        
        //Me fijo si mandaron 
        if (!$request->isXmlHttpRequest() && $request->isMethod('POST') && $recaptchaSecret) {

            $gRecaptchaResponse = $request->request->get('g-recaptcha-response', null, true);
            $remoteIp = $request->getClientIp();
            $recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecret);
            $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
            if ($resp->isSuccess()) {
                return new RedirectResponse($this->container->get('router')->generate('fos_user_security_check'), 307);
            } else {
                $ex = new \Symfony\Component\Security\Core\Exception\AuthenticationException('Invalid Captcha');
                if (class_exists('\Symfony\Component\Security\Core\Security')) {
                    $request->attributes->set(Security::AUTHENTICATION_ERROR, $ex);
                } else {
                    // BC for SF < 2.6
                    $request->attributes->set(SecurityContextInterface::AUTHENTICATION_ERROR, $ex);
                }
            }
        }


        if (class_exists('\Symfony\Component\Security\Core\Security')) {
            $authErrorKey = Security::AUTHENTICATION_ERROR;
            $lastUsernameKey = Security::LAST_USERNAME;
        } else {
            // BC for SF < 2.6
            $authErrorKey = SecurityContextInterface::AUTHENTICATION_ERROR;
            $lastUsernameKey = SecurityContextInterface::LAST_USERNAME;
        }

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        if ($this->has('security.csrf.token_manager')) {
            $csrfToken = $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue();
        } else {
            // BC for SF < 2.4
            $csrfToken = $this->has('form.csrf_provider') ? $this->get('form.csrf_provider')->generateCsrfToken('authenticate') : null;
        }

        if ($request->isXmlHttpRequest()) {

            $response = array(
                'logged' => false,
                'form' => $this->renderAjaxLogin(array(
                    'last_username' => $lastUsername,
                    'error' => $error,
                    'csrf_token' => $csrfToken,
                ))
            );

            return new Response(json_encode($response), 200, array('Content-Type' => 'application/json'));
        }

        return $this->renderLogin(array(
                    'last_username' => $lastUsername,
                    'error' => $error,
                    'csrf_token' => $csrfToken,
                    'recaptchaSiteKey' => $recaptchaSiteKey,
                    'formUrl' => $formUrl
        ));
    }

    /**
     * Renders the login template with the given parameters. Overwrite this function in
     * an extended controller to provide additional data for the login template.
     *
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderAjaxLogin(array $data) {
        return $this->renderView('CoobixUserBundle:Security:ajax_login.html.twig', $data);
    }

}
