<?php

namespace Coobix\UserBundle\Admin\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type as Type;

class Sf3UserListSearchType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', Type\TextType::class, array(
                    'label' => 'Nombre', 
                    'required' => false,
                ))
                ->add('username', Type\TextType::class, array(
                    'label' => 'Nombre de usuario', 
                    'required' => false,
                ))
        ;
    }
     /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
                //'data_class' => 'Coobix\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'list_search';
    }

}
