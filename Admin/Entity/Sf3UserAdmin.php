<?php

namespace Coobix\UserBundle\Admin\Entity;

use Coobix\AdminBundle\Entity\Admin;
use Coobix\UserBundle\Form\Sf3UserEditType as EditType;
use Coobix\UserBundle\Admin\Form\Sf3UserListSearchType as ListSearchType;

class Sf3UserAdmin extends Admin
{

    protected $changeUserRolesForm;

    public function __construct($class) {
        $this->setEditForm(EditType::class);
        $this->setListSearchForm(ListSearchType::class);
        $this->setListTemplate('CoobixUserBundle:User:Admin/list.html.twig');
        $this->setShowTemplate('CoobixUserBundle:User:Admin/show.html.twig');

        parent::__construct($class);
    }

    public function setChangeUserRolesForm($changeUserRolesForm) {
        $this->changeUserRolesForm = $changeUserRolesForm;

        return $this;
    }

    public function getChangeUserRolesForm() {
        return $this->changeUserRolesForm;
    }

}
