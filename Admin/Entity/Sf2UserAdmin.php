<?php

namespace Coobix\UserBundle\Admin\Entity;

use Coobix\AdminBundle\Entity\Admin;
use Coobix\UserBundle\Form\Sf2UserEditType as EditType;
use Coobix\UserBundle\Admin\Form\Sf2UserListSearchType as ListSearchType;

class Sf2UserAdmin extends Admin 
{

    protected $changeUserRolesForm;
    
    public function __construct($class) {
        $this->setEditForm(new EditType());
        $this->setListSearchForm(new ListSearchType());
        parent::__construct($class);
    }

    public function setChangeUserRolesForm($changeUserRolesForm) {
        $this->changeUserRolesForm = $changeUserRolesForm;
        return $this;
    }

    public function getChangeUserRolesForm() {
        return $this->changeUserRolesForm;
    }

}
