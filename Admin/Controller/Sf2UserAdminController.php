<?php

namespace Coobix\UserBundle\Admin\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Coobix\AdminBundle\Entity\BaseList;
use Coobix\AdminBundle\Controller\Sf2AdminController as BaseController;

/**
 * Reparticion Admin controller.
 *
 */
class Sf2UserAdminController extends BaseController {

    /**
     * CRUD *
     * ***** */
    public function listAction(Request $request, $class) {
        
        $admin = $this->get("coobix.admin." . $class);

        $em = $this->getDoctrine()->getManager();

        //CONSULTA PARA RETORNAR LAS ENTIDADES DE LA PAGINA
        //A ESTA CONSULTA LUEGO SE LE AGREGAN FILTROS DE ORDEN, PAGINADO, ETC
        $qb = $em->createQueryBuilder();
        $qb->select('e')->from($admin->getBundleName() . ':' . $admin->getClassName(), 'e');
        $qb->orderBy('e.createdAt', 'DESC');

        $list = new BaseList($this->getDoctrine(), $admin->getClass());
        $list->setQb($qb);

        $listSearchForm = $this->createSearchListForm($admin);
        $listSearchForm->handleRequest($request);
        $listSearchFormIsSubmited = false;
        if ($listSearchForm->isSubmitted()) {
            $listSearchFormIsSubmited = true;
            if ($listSearchForm->isValid()) {
                $list->applyFilters();
            }
        }

        $list->applyOrder();


        //PAGINADOR
        //Clono el query builder para el paginador
        $qbp = clone($list->getQb());
        $qbp->select('count(e.id)');
        $entitiesCount = $qbp->getQuery()->getSingleResult();

        $pager = $this->get('coobix.pager');
        //Url en la cual trabaja el paginador
        $listUrlParams = array(
            'routeName' => 'admin_list',
            'parameters' => array('class' => strtolower($admin->getClassName()))
        );
        //LA URL DEBERIA SER PASADA DIRECTAMENTE Y NO PARA QUE LA HAGA
        //EL PAGINADOR
        $pager->setListUrlParams($listUrlParams);
        $pager->setNumItems($entitiesCount[1]);
        $pager->configure();

        $list->applyLimits();
        $list->getResult();


        $template = 'CoobixUserBundle:User:Admin/list.html.twig';
        return $this->render($template, array(
                    'admin' => $admin,
                    'listSearchFormIsSubmited' => $listSearchFormIsSubmited,
                    'listSearchForm' => $listSearchForm->createView(),
                    'list' => $list,
                    'pager' => $pager,
        ));
    }

    public function showAction(Request $request, $class, $id) {
        $admin = $this->get("coobix.admin." . $class);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('NO SE PUDO ENCONTRAR LA ENTIDAD.');
        }

        $formOptions = array(
            'id' => $id,
            'label' => 'Borrar'
        );
        $deleteForm = $this->createDeleteForm($admin, $formOptions);

        $template = 'CoobixUserBundle:User:Admin/show.html.twig';
        return $this->render($template, array(
                    'entity' => $entity,
                    'admin' => $admin,
                    'deleteForm' => $deleteForm->createView(),
        ));
    }

    public function editAction($class, $id) {
        $admin = $this->get('coobix.admin.' . $class);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('NO SE PUDO ENCONTRAR LA ENTIDAD.');
        }

        $form = $this->createEditForm($admin, $entity);

        $formOptions = array(
            'id' => $id,
            'label' => 'Borrar'
        );
        $deleteForm = $this->createDeleteForm($admin, $formOptions);

        return $this->render('CoobixUserBundle:User:Admin/edit.html.twig', array(
                    'admin' => $admin,
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'deleteForm' => $deleteForm->createView(),
        ));
    }

    public function newAction(Request $request, $class) {
        return $this->redirect($this->generateUrl('fos_user_registration_register'));
    }

    /**
     * Edit user's roles
     */
    public function editRolesAction($class, $id) {

        $admin = $this->get('coobix.admin.' . $class);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($admin->getBundleName() . ':' . $admin->getClassName())->findOneById($id);

        if (!$entity) {
            throw $this->createNotFoundException('NO SE PUDO ENCONTRAR LA ENTIDAD.');
        }

        if ($entity) {
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->findUserByUsername($entity->getUsername());
            if (!is_object($user)) {
                throw new AccessDeniedException('This user does not have access to this section.');
            }
        }

        $form = $this->createForm($admin->getChangeUserRolesForm(), $user, array(
            'action' => $this->generateUrl('user_update_roles_admin', array('class' => $class, 'id' => $user->getId())),
            'method' => 'POST',
        ));

        return $this->render('CoobixUserBundle:User:Admin/change_roles.html.twig', array(
                    'admin' => $admin,
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Edits roles of an existing User entity.
     *
     */
    public function updateRolesAction(Request $request, $class, $id) {

        $admin = $this->get('coobix.admin.' . $class);

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository($admin->getBundleName() . ':' . $admin->getClassName())->findOneById($id);

        if (!$user) {
            throw $this->createNotFoundException('NO SE PUDO ENCONTRAR LA ENTIDAD.');
        }
        
        $form = $this->createForm($admin->getChangeUserRolesForm(), $user, array(
            'action' => $this->generateUrl('user_update_roles_admin', array('class' => $class, 'id' => $user->getId())),
            'method' => 'POST',
        ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formData = $request->request->get('changeuserrolestype');

            $roles = (isset($formData['roles'])) ? $formData['roles'] : array();

            $user->setRoles($roles);
            $em->persist($user);
            $em->flush();
            $this->get('coobix.log')->create('ROLES UPDATE ' . $admin->getClassName() . ': ' . $user . '. ID: ' . $user->getId());
            $this->get('session')->getFlashBag()->add('success', 'REGISTRO MODIFICADO.');
            return $this->redirect($this->generateUrl('admin_show', array('class' => $class, 'id' => $user->getId())));
        }

        return $this->render('CoobixUserBundle:User:Admin/change_roles.html.twig', array(
                    'admin' => $admin,
                    'entity' => $user,
                    'form' => $form->createView(),
        ));
    }

    public function checkSessionAction() {

        $response = json_encode(
                array(
                    'logged' => true
        ));
        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }

}
