<?php

namespace Coobix\UserBundle\Admin\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Coobix\AdminBundle\Entity\BaseList;
use Coobix\AdminBundle\Controller\Sf3AdminController as BaseController;

/**
 * Base User Admin controller.
 *
 */
class Sf3UserAdminController extends BaseController {

    public function newAction(Request $request, $class) {
        return $this->redirect($this->generateUrl('fos_user_registration_register'));
    }

    /**
     * Edit user's roles
     */
    public function editRolesAction($class, $id) {

        $admin = $this->get('coobix.admin.' . $class);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($admin->getEntityShortcutName())->findOneById($id);

        if (!$entity) {
            throw $this->createNotFoundException('NO SE PUDO ENCONTRAR LA ENTIDAD.');
        }

        if ($entity) {
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->findUserByUsername($entity->getUsername());
            if (!is_object($user)) {
                throw new AccessDeniedException('This user does not have access to this section.');
            }
        }

        $form = $this->createForm($admin->getChangeUserRolesForm(), $user, array(
            'action' => $this->generateUrl('user_update_roles_admin', array('class' => $class, 'id' => $user->getId())),
            'method' => 'POST',
        ));

        return $this->render('CoobixUserBundle:User:Admin/change_roles.html.twig', array(
                    'admin' => $admin,
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Edits roles of an existing User entity.
     *
     */
    public function updateRolesAction(Request $request, $class, $id) {

        $admin = $this->get('coobix.admin.' . $class);

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository($admin->getEntityShortcutName())->findOneById($id);

        if (!$user) {
            throw $this->createNotFoundException('NO SE PUDO ENCONTRAR LA ENTIDAD.');
        }
        
        $form = $this->createForm($admin->getChangeUserRolesForm(), $user, array(
            'action' => $this->generateUrl('user_update_roles_admin', array('class' => $class, 'id' => $user->getId())),
            'method' => 'POST',
        ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formData = $request->request->get('changeuserrolestype');

            $roles = (isset($formData['roles'])) ? $formData['roles'] : array();

            $user->setRoles($roles);
            $em->persist($user);
            $em->flush();
            $this->get('coobix.log')->create('ROLES UPDATE ' . $admin->getClassName() . ': ' . $user . '. ID: ' . $user->getId());
            $this->get('session')->getFlashBag()->add('success', 'REGISTRO MODIFICADO.');
            return $this->redirect($this->generateUrl('admin_show', array('class' => $class, 'id' => $user->getId())));
        }

        return $this->render('CoobixUserBundle:User:Admin/change_roles.html.twig', array(
                    'admin' => $admin,
                    'entity' => $user,
                    'form' => $form->createView(),
        ));
    }

    public function checkSessionAction() {

        $response = json_encode(
                array(
                    'logged' => true
        ));
        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }

}
