<?php

namespace Coobix\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class PasswordResettingListener implements EventSubscriberInterface {

    private $router;
    private $log;

    public function __construct(UrlGeneratorInterface $router, $log) {
        $this->router = $router;
        $this->log = $log;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents() {
        return array(
            FOSUserEvents::RESETTING_RESET_SUCCESS => 'onPasswordResettingSuccess',
        );
    }

    public function onPasswordResettingSuccess(FormEvent $event) {

        $user = $event->getForm()->getData();
        // #-CI esto lo pongo en un try por que en las custom_client
        //no puede escribir por que no esta logueado
        try {
            $this->log->create('PASSWORD RESET User: ' . $user . '. ID: ' . $user->getId());
        } catch (\Exception $e) {     
        }
        
        $url = $this->router->generate('fos_user_resetting_success');

        $event->setResponse(new RedirectResponse($url));
    }

}
