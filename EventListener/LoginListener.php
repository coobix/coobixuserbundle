<?php

namespace Coobix\UserBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\SecurityEvents;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Listener responsible to create the BD when the email is confirmed
 */
class LoginListener implements EventSubscriberInterface {

    private $log;

    public function __construct($log) {

        $this->log = $log;
    }

    public static function getSubscribedEvents() {
        return array(
            SecurityEvents::INTERACTIVE_LOGIN => 'onInteractiveLogin',
        );
    }

    public function onInteractiveLogin(InteractiveLoginEvent $event) {

        $user = $event->getAuthenticationToken()->getUser();

        $this->log->create('USER LOGED User: ' . $user . '. ID: ' . $user->getId());
    }

}
