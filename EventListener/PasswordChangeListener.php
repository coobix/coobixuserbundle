<?php

namespace Coobix\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\Response;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class PasswordChangeListener implements EventSubscriberInterface {

    private $router;
    private $log;

    public function __construct(UrlGeneratorInterface $router, $log) {
        $this->router = $router;
        $this->log = $log;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents() {
        return array(
            FOSUserEvents::CHANGE_PASSWORD_SUCCESS => 'onChangePasswordSuccess',
        );
    }

    public function onChangePasswordSuccess(FormEvent $event) {

        $user = $event->getForm()->getData();

        $this->log->create('PASSWORD CHANGED User: ' . $user . '. ID: ' . $user->getId());

        $url = $this->router->generate('user_profile_account');

        $response = new RedirectResponse($url);

        $event->setResponse($response);
    }

}
