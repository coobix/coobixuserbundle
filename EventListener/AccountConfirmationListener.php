<?php

namespace Coobix\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Security\LoginManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Listener responsible to create the BD when the email is confirmed
 */
class AccountConfirmationListener implements EventSubscriberInterface {

    private $log;
    private $securityTokenStorage;

    public function __construct($log, $securityTokenStorage ) {
        $this->log = $log;
        $this->securityTokenStorage = $securityTokenStorage;
    }

    public static function getSubscribedEvents() {
        return array(
            //Este es cuando confirman el email
            FOSUserEvents::REGISTRATION_CONFIRMED => 'onRegistrationConfirmed',
                //Este es cuando se registran
                //FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
                //Cuando se completa la registración
                //FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
        );
        //REGISTRATION_CONFIRMED
    }

    public function onRegistrationConfirmed(FilterUserResponseEvent $event) {

        $token = $this->securityTokenStorage->getToken();
        $user = $token->getUser();

        $this->log->create('USER CONFIRMED User: ' . $user . '. ID: ' . $user->getId());
        
    }

}
